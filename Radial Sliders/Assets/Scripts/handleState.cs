﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class handleState : MonoBehaviour
{

    public GameObject object1;
    public GameObject object2;
    public GameObject object1FullscreenBkg;
    public GameObject object2FullscreenBkg;
    public GameObject iconCloseX;
    public bool object1IsFocused;
    public bool object2IsFocused;
    float fullScreenBkgScaleActive = 25;
    float fullScreenBkgScaleInactive = 1;
    public GameObject volSliderRoom;
    public GameObject volSliderObject1;
    public GameObject timeSliderObject1;
    public GameObject volSliderObject2;
    public GameObject timeSliderObject2;
    public GameObject transportControls;
    public GameObject drawerIcon;
    public Vector3 object1LastDraggedPos;
    
    public GameObject roomCenter;

    float allSlidersStrokeWeight;

    // Start is called before the first frame update
    void Start()
    {
        allSlidersStrokeWeight = 0.98f; //1 = no stroke, lower = fatter
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setObject1FocusState() {

        if (object1IsFocused == true)
        {
            object1LastDraggedPos = object1.transform.position;
            object1.transform.DOScale(new Vector3(3.815f, 3.815f, 3.815f), 0.4f).SetEase(Ease.OutBack);
            object1.transform.DOMove(roomCenter.transform.position, 0.4f).SetEase(Ease.OutBack);
            object1.GetComponent<SphereCollider>().enabled = false;
            object1.GetComponent<SpriteRenderer>().sortingOrder = 100;

            object2.GetComponent<SpriteRenderer>().sortingOrder = 99;
            object2.transform.DOScale(new Vector3(0, 0, 0), 0.3f);

            iconCloseX.SetActive(true);
            object1FullscreenBkg.transform.DOScale(new Vector3(fullScreenBkgScaleActive, fullScreenBkgScaleActive, fullScreenBkgScaleActive), 0.5f);
            hideRoomSlider();
            Invoke("showObject1Sliders", 0.3f);
        }
        else
        {
            iconCloseX.SetActive(false);
            object1.transform.DOScale(new Vector3(0.9f, 0.9f, 0.9f), 0.4f).SetEase(Ease.OutBack);
            object1.transform.DOMove(object1LastDraggedPos, 0.4f).SetEase(Ease.OutBack);
            object1.GetComponent<SphereCollider>().enabled = true;
            object1FullscreenBkg.transform.DOScale(new Vector3(fullScreenBkgScaleInactive, fullScreenBkgScaleInactive, fullScreenBkgScaleInactive), 0.4f).SetEase(Ease.OutSine);

            object2.transform.DOScale(new Vector3(1, 1, 1), 0.3f);

            Invoke("showRoomSlider", 0.5f);

            hideObjectSliders();

        }

    }

    public void setObject2FocusState()
    {

        if (object2IsFocused == true)
        {
            
            object2.GetComponent<SpriteRenderer>().sortingOrder = 100;
            object1.GetComponent<SpriteRenderer>().sortingOrder = 99;
            object1.transform.DOScale(new Vector3(0, 0, 0), 0.3f);

            object2FullscreenBkg.transform.DOScale(new Vector3(fullScreenBkgScaleActive * 3, fullScreenBkgScaleActive * 3, fullScreenBkgScaleActive * 3), 0.8f);


            iconCloseX.SetActive(true);
            hideRoomSlider();
            Invoke("showObject2Sliders", 0.4f);
        }
        else
        {
            iconCloseX.SetActive(false);
            
            object1.transform.DOScale(new Vector3(0.9f, 0.9f, 0.9f), 0.3f);

            object2FullscreenBkg.transform.DOScale(new Vector3(7.7f, 7.7f, 7.7f), 0.4f).SetEase(Ease.OutSine);


            Invoke("showRoomSlider", 0.5f);

            hideObjectSliders();

        }

    }

    void showObject1Sliders()
    {
        volSliderObject1.SetActive(true);
        timeSliderObject1.SetActive(true);
        transportControls.SetActive(true);
    }

    void showObject2Sliders()
    {
        volSliderObject2.SetActive(true);
        timeSliderObject2.SetActive(true);
        
    }

    void hideObjectSliders()
    {
        volSliderObject1.SetActive(false);
        timeSliderObject1.SetActive(false);
        transportControls.SetActive(false);
        volSliderObject2.SetActive(false);
        timeSliderObject2.SetActive(false);
    }

    void showRoomSlider()
    {
        volSliderRoom.SetActive(true);
        drawerIcon.SetActive(true);
    }

    void hideRoomSlider()
    {
        volSliderRoom.SetActive(false);
        drawerIcon.SetActive(false);
    }
}
