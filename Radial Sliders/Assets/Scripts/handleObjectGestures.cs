﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class handleObjectGestures : MonoBehaviour
{
    public TapGesture singleTap;
    handleState HandleState;

    // Start is called before the first frame update
    void Start()
    {
        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();
        singleTap = gameObject.GetComponent<TapGesture>();
        singleTap.Tapped += SingleTap_Tapped;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SingleTap_Tapped(object sender, System.EventArgs e)
    {
        //Debug.Log("tappity tap");

        if (transform.gameObject.tag == "object1")
        {
            if (HandleState.object1IsFocused == false)
            {
                HandleState.object1IsFocused = true;
                HandleState.setObject1FocusState();
            }
        }

        if (transform.gameObject.tag == "object2")
        {
            if (HandleState.object2IsFocused == false)
            {
                HandleState.object2IsFocused = true;
                HandleState.setObject2FocusState();
            }
        }





    }



}
