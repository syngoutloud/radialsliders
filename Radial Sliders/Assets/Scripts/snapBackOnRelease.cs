﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;

public class snapBackOnRelease : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            transform.DOLocalMove(new Vector3(0, 0, 0), 0.5f).SetEase(Ease.OutBack);
        }

    }

}
