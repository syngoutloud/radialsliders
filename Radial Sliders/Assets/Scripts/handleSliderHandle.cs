﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using Shapes2D;
using DG.Tweening;

public class handleSliderHandle : MonoBehaviour
{

    public GameObject sliderParent;
    public GameObject sliderRailValue;
    public GameObject sliderRail;
    float angle;
    float angleRangeMin;
    float angleRangeMax;
    float totalRange;
    public GameObject sliderHandleParent;
    public GameObject sliderHandleVisible;
    bool isPressed;
    public configureSlider ConfigureSlider;
    public GameObject iconMax;
    public GameObject iconMin;
    public GameObject handleVisible;
    Vector3 handleVisibleStartingScale;
    float sliderRailInactiveOpacity;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;
        GetComponent<PressGesture>().StateChanged += pressHandler;
        Invoke("setAngleRange", 0.2f);
        Invoke("rotate", 0.25f);
        handleVisibleStartingScale = handleVisible.transform.localScale;
        sliderRailInactiveOpacity = 0.1f;
    }

    // Update is called once per frame
    void Update()
    {
        if (isPressed == true && angle < 180)
        {
            rotate();
        }

        setAngleRange();


    }

    void setAngleRange()
    {

        //get angle range, then plus minus from zero
        totalRange = ConfigureSlider.angleRange;

        angleRangeMax = sliderParent.transform.eulerAngles.z + (totalRange / 2);
        angleRangeMin = sliderParent.transform.eulerAngles.z - (totalRange / 2);

        
   
    }

    void rotate()
    {
        //get direction from touch to room center
        Vector3 vectorToTarget = this.transform.position - sliderParent.transform.position;

        //Debug.DrawLine(this.transform.position, sliderParent.transform.position, Color.green, Time.deltaTime, false);

        //get its angle
        angle = (Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg);

        //print("angle = " + angle);

        

        //move the visible handle by rotating its parent
        sliderHandleParent.transform.eulerAngles = new Vector3(0, 0, ClampAngle(angle - 90, angleRangeMin, angleRangeMax));

        //convert this to 0-360 starting/ending at base of circle

        float convertedAngle;

        if (angle - sliderParent.transform.eulerAngles.z < -90)
        {
            convertedAngle = 180 - (270 + angle);
        }
        else
        {
            convertedAngle = -(angle - 90) + 180;
        }

        //print("convertedAngle = " + convertedAngle);

        //draw the slider value rail

        float sliderEdgeMargin = (360 - totalRange) / 2;

        //if (convertedAngle < 360-sliderEdgeMargin && convertedAngle > sliderEdgeMargin)
        //{
            if (sliderParent.transform.eulerAngles.z > 0)
            {
                //print("111");
                sliderRailValue.GetComponent<Shape>().settings.endAngle = 360-((360 - totalRange) / 2);
                sliderRailValue.GetComponent<Shape>().settings.startAngle = convertedAngle + sliderParent.transform.eulerAngles.z;

            }
            else
            {
                //print("222");
            sliderRailValue.GetComponent<Shape>().settings.endAngle = (360 - totalRange) / 2;
            sliderRailValue.GetComponent<Shape>().settings.startAngle = convertedAngle + sliderParent.transform.eulerAngles.z;
            
        }
        // }





    }

    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("slider released!");
            
            transform.position = sliderHandleVisible.transform.position;
            handleVisible.transform.DOScale(handleVisibleStartingScale, 0.3f);
            sliderRailValue.GetComponent<SpriteRenderer>().DOFade(sliderRailInactiveOpacity, 0.4f);
            iconMax.GetComponent<SpriteRenderer>().DOFade(0.4f, 0.4f);
            iconMin.GetComponent<SpriteRenderer>().DOFade(0.4f, 0.4f);
            isPressed = false;
        }

    }

    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("slider pressed!");

            handleVisible.transform.DOScale(handleVisibleStartingScale * 3, 0.1f);
            sliderRailValue.GetComponent<SpriteRenderer>().DOFade(0.8f, 0.2f);
            iconMax.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
            iconMin.GetComponent<SpriteRenderer>().DOFade(1, 0.2f);
            isPressed = true;

        }

    }

    //praise baby jebus for someone solving rotation probs outside 0-360 range
    //https://answers.unity.com/questions/141775/limit-local-rotation.html

    protected float ClampAngle(float angle, float min, float max)
    {

        angle = NormalizeAngle(angle);
        if (angle > sliderParent.transform.localEulerAngles.z + 180)
        {
            angle -= 360;
        }
        else if (angle < sliderParent.transform.localEulerAngles.z - 180)
        {
            angle += 360;
        }

        min = NormalizeAngle(min);
        if (min > sliderParent.transform.localEulerAngles.z + 180)
        {
            min -= 360;
        }
        else if (min < sliderParent.transform.localEulerAngles.z - 180)
        {
            min += 360;
        }

        max = NormalizeAngle(max);
        if (max > sliderParent.transform.localEulerAngles.z + 180)
        {
            max -= 360;
        }
        else if (max < sliderParent.transform.localEulerAngles.z - 180)
        {
            max += 360;
        }

        // Aim is, convert angles to -180 until 180.
        return Mathf.Clamp(angle, min, max);
    }

    /** If angles over 360 or under 360 degree, then normalize them.
     */
    protected float NormalizeAngle(float angle)
    {
        while (angle > 360)
            angle -= 360;
        while (angle < 0)
            angle += 360;
        return angle;
    }
}
