﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class handleButton : MonoBehaviour
{

    //handleTransition HandleTransition;
    //handleSettingsInfoPanel HandleSettingsInfoPanel;
    //handleStateHardball HandleStateHardball;
    //handleStateJourney HandleStateJourney;

    Vector3 startingScale;
    float pressScale = 0.95f;
    float pressDuration = 0.05f;
    float releaseDuration = 0.25f;
    //float transitionOutDelay = 0.25f;
    //float changeSceneDelay = 1f;

    //AudioSource buttonDown;
    //AudioSource buttonUp;

    //Scene currentScene;

    handleState HandleState;

    




    // Use this for initialization
    void Start()
    {

        HandleState = GameObject.Find("GameManager").GetComponent<handleState>();

        //currentScene = SceneManager.GetActiveScene();

        //buttonDown = GameObject.Find("sfxButtonDown").GetComponent<AudioSource>();
        //buttonUp = GameObject.Find("sfxButtonUp").GetComponent<AudioSource>();

        //HandleTransition = GameObject.Find("gameManager").GetComponent<handleTransition>();

        //HandleSettingsInfoPanel = GameObject.Find("gameManager").GetComponent<handleSettingsInfoPanel>();

        //HandleStateHardball = GameObject.Find("gameManager").GetComponent<handleStateHardball>();

        //HandleStateJourney = GameObject.Find("gameManager").GetComponent<handleStateJourney>();

        GetComponent<ReleaseGesture>().StateChanged += releaseHandler;

        GetComponent<PressGesture>().StateChanged += pressHandler;

        Invoke("getStartingScale", 0.2f); //need a lil delay here as we're setting initial scale in handleScreenSize



    }

    void getStartingScale()
    {
        startingScale = transform.localScale;
    }



    private void releaseHandler(object sender, GestureStateChangeEventArgs e)
    {

        ReleaseGesture gesture = sender as ReleaseGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
            //print("button released!");

            //buttonUp.Play();

            if (gameObject.GetComponent<SpriteRenderer>() != null)
            {
                gameObject.GetComponent<SpriteRenderer>().transform.DOScaleX(startingScale.x, releaseDuration).SetEase(Ease.OutBack);
                gameObject.GetComponent<SpriteRenderer>().transform.DOScaleY(startingScale.y, releaseDuration).SetEase(Ease.OutBack);
                gameObject.GetComponent<SpriteRenderer>().transform.DOScaleZ(startingScale.z, releaseDuration).SetEase(Ease.OutBack);

            }
            else
            {
                gameObject.transform.DOScaleX(startingScale.x, releaseDuration).SetEase(Ease.OutBack);
                gameObject.transform.DOScaleY(startingScale.y, releaseDuration).SetEase(Ease.OutBack);
                gameObject.transform.DOScaleZ(startingScale.z, releaseDuration).SetEase(Ease.OutBack);

            }




            if (gesture.gameObject.tag == "iconCloseX")
            {
                //print("close");

                if (HandleState.object1IsFocused == true)
                {
                    HandleState.object1IsFocused = false;
                    HandleState.setObject1FocusState();
                }

                if (HandleState.object2IsFocused == true)
                {
                    HandleState.object2IsFocused = false;
                    HandleState.setObject2FocusState();
                }



            }

            

            
            

        }

    }

    



    private void pressHandler(object sender, GestureStateChangeEventArgs e)
    {

        PressGesture gesture = sender as PressGesture;

        if (e.State == Gesture.GestureState.Ended)
        {
           // print("button pressed!");

            //buttonDown.Play();

            gameObject.transform.DOScaleX(startingScale.x * pressScale, pressDuration);
            gameObject.transform.DOScaleY(startingScale.y * pressScale, pressDuration);
            gameObject.transform.DOScaleZ(startingScale.z * pressScale, pressDuration);



        }

    }

}



